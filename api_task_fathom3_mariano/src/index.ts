import Fastify, { FastifyRequest, FastifyReply } from "fastify";
import cors from "@fastify/cors";
import fjwt, { JWT } from "@fastify/jwt";
import swagger from "@fastify/swagger";
import swaggerUI from "@fastify/swagger-ui";
import { withRefResolver } from "fastify-zod";
import userRoutes from "./user/user.route";
import { userSchemas } from "./user/user.schema";
import * as dotenv from "dotenv";
dotenv.config();

// Main env conf interface
interface ENV {
  host: string;
  port: number;
}

// FastifyRequest add JWT
declare module "fastify" {
  interface FastifyRequest {
    jwt: JWT;
  }
  export interface FastifyInstance {
    authenticate: any;
  }
}

/**
 * Run the server!
 */
const start = async () => {
  const fastify = Fastify({ logger: true });
  try {
    // Enable cors
    await fastify.register(cors);

    // Register JWT
    fastify.register(fjwt, {
      secret: process.env.JWT_SECRET ? process.env.JWT_SECRET : "secret",
    });

    // Verify JWT
    fastify.decorate(
      "authenticate",
      async (request: FastifyRequest, reply: FastifyReply) => {
        try {
          await request.jwtVerify();
        } catch (e) {
          return reply.send(e);
        }
      }
    );

    // preHandler fastify assign JWT
    fastify.addHook("preHandler", (req, reply, next) => {
      req.jwt = fastify.jwt;
      return next();
    });

    for (const schema of [...userSchemas]) {
      // Add schemas for swagger
      fastify.addSchema(schema);
    }

    // Register Swagger + Conf
    fastify.register(
      swagger,
      withRefResolver({
        openapi: {
          info: {
            title: "API Task Mariano Swagger",
            description: "CRUD User Task",
            version: "1.0.0",
          },
          components: {
            securitySchemes: {
              BearerAuth: { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' }
            },
          }
        },
      })
    );

    // Register SwaggerUi
    await fastify.register(swaggerUI, {
      routePrefix: "/docs",
      staticCSP: false,
    });

    // Register users routes
    await fastify.register(userRoutes, { prefix: "users" });

    // Server HOST and PORT conf
    const conf: ENV = {
      host: process.env.HOST ? process.env.HOST : "0.0.0.0",
      port: process.env.PORT ? Number(process.env.PORT) : 3000,
    };
    
    // Listen HOST and PORT
    await fastify.listen(conf);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
