import { FastifyInstance } from "fastify";
import { $ref, userIdParam } from "./user.schema";
import {
  registerUserHandler,
  getUsersHandler,
  getUsersByIdHandler,
  updateUserHandler,
  deleteUserHandler,
  loginUserHandler,
} from "./user.controller";

// All users routes
async function userRoutes(fastify: FastifyInstance) {

  // Get user by id
  fastify.get(
    "/:id",
    {
      preHandler: [fastify.authenticate],
      schema: {
        description: "Get user by id",
        params: userIdParam,
        security: [
          {
            BearerAuth: []
          }
        ],
        response: {
          200: $ref("getUsersSchemaResponse"),
        },
      },
    },
    getUsersByIdHandler
  );

  // Get users
  fastify.get(
    "/",
    {
      preHandler: [fastify.authenticate],
      schema: {
        description: "Get users",
        security: [
          {
            BearerAuth: []
          }
        ],
        response: {
          200: $ref("getUsersSchemaResponse"),
        },
      },
    },
    getUsersHandler
  );

  // Create user
  fastify.post(
    "/",
    {
      schema: {
        description: "Create a user",
        body: $ref("userSchema"),
        response: {
          200: $ref("userSchemaResponse"),
        },
      },
    },
    registerUserHandler
  );

  // Update user
  fastify.patch(
    "/:id",
    {
      preHandler: [fastify.authenticate],
      schema: {
        description: "Update user by id",
        params: userIdParam,
        body: $ref("updateUserSchema"),
        security: [
          {
            BearerAuth: []
          }
        ],
        response: {
          200: $ref("getUserSchemaResponse"),
        },
      },
    },
    updateUserHandler
  );

  // Delete user
  fastify.delete(
    "/:id",
    {
      preHandler: [fastify.authenticate],
      schema: {
        description: "Delete user by id",
        params: userIdParam,
        security: [
          {
            BearerAuth: []
          }
        ],
        response: {
          200: $ref("getUserSchemaResponse"),
        },
      },
    },
    deleteUserHandler
  );

  // Login user
  fastify.post(
    "/login",
    {
      schema: {
        description: "Login email and password",
        body: $ref("loginSchema"),
        response: {
          200: $ref("userSchemaResponse"),
        },
      },
    },
    loginUserHandler
  );
}

export default userRoutes;
