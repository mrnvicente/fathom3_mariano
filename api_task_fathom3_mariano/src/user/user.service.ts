import prisma from "../utils/prisma";
import { hashPassword } from '../utils/utils'
import { UserType, UpdateUserType } from "./user.schema";

export async function createUser(data: UserType) {
  // Hash password
  data.password = await hashPassword(data.password);
  // Insert User
  const user = await prisma.user.create({
    data,
  });
  // Return user
  return user;
}

export async function findUserByEmail(email: string) {
  // Find user by email
  return prisma.user.findUnique({
    where: {
      email,
    },
  });
}

export async function findUsers() {
  // Select all
  return prisma.user.findMany();
}

export async function findUserById(id: number) {
  // Find user by Id
  return prisma.user.findMany({
    where: {
      id,
    },
  });
}

export async function updateUserById(id: number, data: UpdateUserType) {
  // If password hash
  if (data.password) {
    data.password = await hashPassword(data.password);
  }
  // Update user
  return prisma.user.update({
    where: { id },
    data,
  });
}

export async function deleteUserById(id: number) {
  // Delete user by Id
  return prisma.user.delete({
    where: { id },
  });
}
