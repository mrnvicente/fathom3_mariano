import joi from "joi";

// Validate id
export async function validateGetUserById(body: object) {
  const validation = joi.object({ id: joi.number().required() });
  return await validation.validateAsync(body);
}
