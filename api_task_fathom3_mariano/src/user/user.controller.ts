import { FastifyReply, FastifyRequest } from "fastify";
import {
  UserType,
  UpdateUserType,
  LoginType,
  userSchemaResponseType,
} from "./user.schema";
import { validateGetUserById } from "./user.validator";
import {
  checkIfUserEmailExists,
  checkIfUserIdExists,
  checkPassword,
} from "../utils/utils";
import {
  createUser,
  findUsers,
  findUserById,
  updateUserById,
  deleteUserById,
  findUserByEmail,
} from "./user.service";

// Get user by id controller
export async function getUsersByIdHandler(
  request: FastifyRequest<{
    Params: { id: string };
  }>,
  reply: FastifyReply
) {
  try {
    // Validate id
    const { id } = await validateGetUserById(request.params);
    // Throw error if user not exists by id
    await checkIfUserIdExists(id);
    // Find user by id
    const user = await findUserById(id);
    return reply.code(200).send(user);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}

// Get user controller
export async function getUsersHandler(
  request: FastifyRequest,
  reply: FastifyReply
) {
  try {
    // Select all users
    const users = await findUsers();
    return reply.code(200).send(users);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}

// Create user controller
export async function registerUserHandler(
  request: FastifyRequest<{
    Body: UserType;
  }>,
  reply: FastifyReply
) {
  try {
    const body = request.body;
    // Throw error if user exists by email
    await checkIfUserEmailExists(body.email);
    // Create user
    const user = await createUser(body);
    // Response
    const res: userSchemaResponseType = {
      ...user,
      ...{ accessToken: request.jwt.sign(user) },
    };
    return reply.code(200).send(res);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}

// Update user controller
export async function updateUserHandler(
  request: FastifyRequest<{
    Params: { id: string };
    Body: UpdateUserType;
  }>,
  reply: FastifyReply
) {
  try {
    // Validate id
    const { id } = await validateGetUserById(request.params);
    const body = request.body;
    // Throw error if user not exists by id
    await checkIfUserIdExists(id);
    if (body.email) {
      // Throw error if user exists by email
      await checkIfUserEmailExists(body.email, id);
    }
    // Update user
    const user = await updateUserById(id, body);
    return reply.code(200).send(user);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}

// Delete user controller
export async function deleteUserHandler(
  request: FastifyRequest<{
    Params: { id: string };
  }>,
  reply: FastifyReply
) {
  try {
    // Validate id
    const { id } = await validateGetUserById(request.params);
    // Throw error if user not exists by id
    await checkIfUserIdExists(id);
    // Delete user
    const user = await deleteUserById(id);
    return reply.code(200).send(user);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}

// Create user controller
export async function loginUserHandler(
  request: FastifyRequest<{
    Body: LoginType;
  }>,
  reply: FastifyReply
) {
  try {
    const body = request.body;
    // Throw error if user exists by email
    const user = await findUserByEmail(body.email);
    if (user) {
      await checkPassword(body.password, user.password);
    } else {
      throw new Error(`The user with the email ${body.email} does not exist`);
    }
    // Response
    const res: userSchemaResponseType = {
      ...user,
      ...{ accessToken: request.jwt.sign(user) },
    };
    return reply.code(200).send(res);
  } catch (e) {
    console.log(e);
    return reply.code(500).send(e);
  }
}
