import { z } from "zod";
import { buildJsonSchemas } from "fastify-zod";

// Create user params validation
const createUserValidation = {
  email: z.string().email(),
  name: z.string(),
  password: z.string(),
  age: z.number(),
  address: z.string().optional().nullable(),
};

// Update user params validation (all optional)
const updateUserValidation = {
  email: z.string().email().optional(),
  name: z.string().optional(),
  password: z.string().optional(),
  age: z.number().optional(),
  address: z.string().optional().nullable(),
};

// Login params validation
const loginCoreValidation = {
  email: z.string().email(),
  password: z.string(),
};

// Create user schema
const userSchema = z.object({
  ...createUserValidation,
});

// Update user schema
const updateUserSchema = z.object({
  ...updateUserValidation,
});

// Login schema
const loginSchema = z.object({
  email: z.string().email(),
  password: z.string(),
});

// Create user and Login response
const userSchemaResponse = z.object({
  id: z.number(),
  ...createUserValidation,
  accessToken: z.string(),
});

// Get Path Delete (by id) user response
const getUserSchemaResponse = z.object({
  id: z.number(),
  ...createUserValidation,
});

// Get user response (Array<getUserSchemaResponse>)
const getUsersSchemaResponse = z.array(getUserSchemaResponse);

// User id Param
export const userIdParam = {
  description: "User Id",
  type: "object",
  properties: {
    id: { type: "string" },
  },
};

// Types
// User Res
export type userSchemaResponseType = z.infer<typeof userSchemaResponse>;

// User Post
export type UserType = z.infer<typeof userSchema>;

// User Update
export type UpdateUserType = z.infer<typeof updateUserSchema>;

// Login
export type LoginType = z.infer<typeof loginSchema>;

export const { schemas: userSchemas, $ref } = buildJsonSchemas({
  userSchema,
  userSchemaResponse,
  getUserSchemaResponse,
  getUsersSchemaResponse,
  updateUserSchema,
  loginSchema
});
