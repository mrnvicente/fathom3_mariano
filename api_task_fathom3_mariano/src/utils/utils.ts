import bcrypt from "bcrypt";
import { findUserByEmail, findUserById } from "../user/user.service";

export async function hashPassword(password: string) {
  // Generate a salt
  const salt = await bcrypt.genSalt(10);
  // Hash password
  return await bcrypt.hash(password, salt);
}

export async function checkPassword(password: string, passwordDb: string) {
  // Check if user pass from the db is equal than the user object pass
  const valid = await bcrypt.compare(password, passwordDb);
  if (!valid) {
    throw new Error("Incorrect password");
  }
  return true;
}

// Throw error if user exists by email
export async function checkIfUserEmailExists(email: string, id?: number) {
  const user = await findUserByEmail(email);
  if (user && id && (user.id === id)) {
    return user
  } else if (user) {
    throw new Error("Email already exist");
  }
  return user;
}

// Throw error if user not exists by id
export async function checkIfUserIdExists(id: number) {
  const user = await findUserById(id);
  if (user.length <= 0) {
    throw new Error(`The user with id ${id} does not exist`);
  }
  return user;
}
