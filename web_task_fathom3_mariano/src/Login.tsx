import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // Navigate for redirect
  const navigate = useNavigate();

  // If localStorage user info
  // Redirect to main page (/)
  useEffect(() => {
    if (localStorage.getItem("user-info")) {
      navigate("/");
    }
  }, []);

  async function login(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    // Get email and password actual value
    let item = { email, password };
    // Login user
    let result = await fetch(process.env.REACT_APP_API_URL + "users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(item),
    })
      .then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          // Save API response to user info localStorage
          localStorage.setItem("user-info", JSON.stringify(data));
          navigate("/");
        } else {
          // API ERR
          alert(data.message);
        }
      })
      .catch((error) => {
        // API ERR
        alert("API ERR: " + error.message);
      });
  }

  return (
    <div>
      <Header />
      <br />
      <h1>Login</h1>
      <br />
      <div className="col-sm-6 offset-sm-3">
        <form onSubmit={login}>
          <input
            type="email"
            placeholder="email"
            required
            onChange={(e) => setEmail(e.target.value)}
            className="form-control"
          />
          <br />
          <input
            type="password"
            placeholder="password"
            required
            minLength={4}
            onChange={(e) => setPassword(e.target.value)}
            className="form-control"
          />
          <br />
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </div>
  );
}

export default Login;
