import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Header from "./Header";

function Register() {
  // If localStorage user info
  // Redirect to main page (/)
  useEffect(() => {
    if (localStorage.getItem("user-info")) {
      navigate("/");
    }
  }, []);

  const [email, setEamil] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [age, setAge] = useState("");
  // Navigate for redirect
  const navigate = useNavigate();

  async function signUp(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    // Get email, password, name, age, address actual value
    let item = { email, password, name, age, address };
    // Create user
    let result = await fetch(process.env.REACT_APP_API_URL + "users", {
      method: "POST",
      body: JSON.stringify(item),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          // Save API response to user info localStorage
          localStorage.setItem("user-info", JSON.stringify(data));
          navigate("/");
        } else {
          // API ERR
          alert(data.message);
        }
      })
      .catch((error) => {
        // API ERR
        alert("API ERR: " + error.message);
      });
  }

  return (
    <>
      <Header />
      <br />
      <div className="col-sm-6 offset-sm-3">
        <h1>Register</h1>
        <br />
        <form onSubmit={signUp}>
          <input
            type="email"
            value={email}
            required
            onChange={(e) => setEamil(e.target.value)}
            className="form-control"
            placeholder="email"
          />
          <br />
          <input
            type="password"
            value={password}
            minLength={4}
            required
            onChange={(e) => setPassword(e.target.value)}
            className="form-control"
            placeholder="password"
          />
          <br />
          <input
            type="text"
            value={name}
            minLength={1}
            required
            onChange={(e) => setName(e.target.value)}
            className="form-control"
            placeholder="name"
          />
          <br />
          <input
            type="number"
            value={age}
            min="1"
            max="99"
            required
            onChange={(e) => setAge(e.target.value)}
            className="form-control"
            placeholder="age"
          />
          <br />
          <input
            type="text"
            value={address}
            minLength={4}
            onChange={(e) => setAddress(e.target.value)}
            className="form-control"
            placeholder="address"
          />
          <br />
          <button type="submit" className="btn btn-primary">
            Sign Up
          </button>
        </form>
      </div>
    </>
  );
}

export default Register;
