// API URL
export const API_URL: string = process.env.REACT_APP_API_URL
  ? process.env.REACT_APP_API_URL
  : "";

// User Info data
export interface UserInfo {
  id: number;
  email: string;
  name: string;
  password: string;
  age: number;
  address: string;
  accessToken: string;
}

interface keyType {
  [key: number]: string;
}

// User data
export interface User extends keyType {
  id?: number;
  email?: string;
  name?: string;
  password?: string;
  age?: number;
  address?: string;
}

// State for User Class Component
export type stateProps = {
  data: Array<User>;
  updateModal: boolean;
  deleteModal: boolean;
  redirect: boolean;
  form: User;
};
