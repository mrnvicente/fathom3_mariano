import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function Protected(props: any) {
  let Cmp = props.Cmp;
  // Navigate for redirect
  const navigate = useNavigate();

  // If not localStorage user info
  // Redirect to page (/register)
  useEffect(() => {
    if (!localStorage.getItem("user-info")) {
      navigate("/register");
    }
  }, []);

  return (
    <div>
      <Cmp />
    </div>
  );
}

export default Protected;
