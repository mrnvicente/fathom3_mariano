import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { UserInfo } from "./web.schema";

function Header() {
  // Navigate for redirect
  let navigate = useNavigate();
  // Localstorage user info
  let user: UserInfo = JSON.parse(localStorage.getItem("user-info") ?? "{}");

  // Log out function
  function logout() {
    localStorage.clear();
    navigate("/login");
  }

  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">FATHOM3 MARIANO</Navbar.Brand>
        <Nav className="mr-auto nav_bar_wrapper">
          {localStorage.getItem("user-info") ? (
            <>
              <Link to="/">USER LIST</Link>
            </>
          ) : (
            <>
              <Link to="/login">Login </Link>
              <Link to="/register">Register </Link>
            </>
          )}
        </Nav>
        <Nav>
          {localStorage.getItem("user-info") ? (
            <NavDropdown title={user && user.name}>
              <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
            </NavDropdown>
          ) : null}
        </Nav>
      </Navbar>
    </div>
  );
}

export default Header;
