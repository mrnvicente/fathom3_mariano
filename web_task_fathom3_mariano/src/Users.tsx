import Header from "./Header";
import { Component } from "react";
import "./App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { Navigate } from "react-router-dom";
import { API_URL, User, UserInfo, stateProps } from "./web.schema";

// User object for save params values before update
let formBeforeUpdate: User;

// Main Page Class Component
class Users extends Component<stateProps> {
  // State Info
  // data: [] => All user data
  // updateModal: bool => For open close update modal
  // deleteModal: bool => For open close delete modal
  // redirect: bool => For redirect to (/register) after user delete
  // form: User => User object
  state: stateProps = {
    data: [],
    updateModal: false,
    deleteModal: false,
    redirect: false,
    form: {
      id: 0,
      email: "",
      password: "",
      name: "",
      age: 0,
      address: "",
    },
  };

  // localStorage user info
  thisUser: UserInfo = JSON.parse(localStorage.getItem("user-info") ?? "{}");

  getReq = async () => {
    // Get all users
    await fetch(API_URL + "users", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${this.thisUser.accessToken}`,
      },
    })
      .then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          // If API response set state user data
          this.setState({ data });
        } else {
          // API ERR
          alert(data.message);
        }
      })
      .catch((error) => {
        // API ERR
        alert("API ERR: " + error.message);
      });
  };

  patchReq = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // Patch user data
    // State actual Form data
    await fetch(API_URL + "users/" + this.state.form.id, {
      method: "PATCH",
      body: JSON.stringify(this.state.form),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.thisUser.accessToken}`,
      },
    })
      .then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          // If API response close modal and get users
          this.updateModal();
          this.getReq();
        } else {
          // API ERR
          alert(data.message);
        }
      })
      .catch((error) => {
        // API ERR
        alert("API ERR: " + error.message);
      });
  };

  deleteReq = async () => {
    // Delete user by id
    await fetch(API_URL + "users/" + this.state.form.id, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${this.thisUser.accessToken}`,
      },
    })
      .then(async (response) => {
        const data = await response.json();
        if (response.ok) {
          // If API response delete localStorage data and redirect to (/register)
          this.setState({ deleteModal: false });
          this.setRedirect();
        } else {
          // API ERR
          alert(data.message);
        }
      })
      .catch((error) => {
        // API ERR
        alert("API ERR: " + error.message);
      });
  };

  updateModal = () => {
    // Toggle between True and False (updateModal: bool)
    this.setState({ updateModal: !this.state.updateModal });
  };

  selectUser = (user: User) => {
    // Save user data before any changes in state Form
    formBeforeUpdate = {
      id: user.id,
      email: user.email,
      password: user.password,
      name: user.name,
      address: user.address,
      age: user.age,
    };

    // Set form data
    this.setState({
      form: formBeforeUpdate,
    });
  };

  handleChange = async (e: any) => {
    e.persist();
    // Any form changes, setState form value
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });

    // Delete not modificated keys
    // For not update unmodified data
    for (let key in formBeforeUpdate) {
      if (formBeforeUpdate.hasOwnProperty(key)) {
        if (formBeforeUpdate[key] === this.state.form[key]) {
          delete this.state.form[key];
        }
      }
    }
  };

  // For redirect
  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  // Clear user info data and redirect to (/register) Func
  renderRedirect = () => {
    if (this.state.redirect) {
      localStorage.clear();
      return <Navigate to="/register" />;
    }
  };

  // 1º Get users data
  componentDidMount() {
    this.getReq();
  }

  render() {
    const { form } = this.state;
    return (
      <div className="App">
        <Header />
        <br />
        <h1>User List</h1>
        <br />
        <div>{this.renderRedirect()}</div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Email</th>
              <th>Name</th>
              <th>Address</th>
              <th>Age</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((user) => {
              return (
                <tr>
                  <td>{user.id ? user.id : "-"}</td>
                  <td>{user.email ? user.email : "-"}</td>
                  <td>{user.name ? user.name : "-"}</td>
                  <td>{user.address ? user.address : "-"}</td>
                  <td>{user.age ? user.age : "-"}</td>
                  {user.id === this.thisUser.id ? (
                    <td>
                      <button
                        className="btn btn-primary"
                        onClick={() => {
                          this.selectUser(user);
                          this.updateModal();
                        }}
                      >
                        <FontAwesomeIcon icon={faEdit} />
                      </button>
                      {"   "}
                      <button
                        className="btn btn-danger"
                        onClick={() => {
                          this.selectUser(user);
                          this.setState({ deleteModal: true });
                        }}
                      >
                        <FontAwesomeIcon icon={faTrashAlt} />
                      </button>
                    </td>
                  ) : (
                    <td>
                      <button disabled className="btn btn-primary">
                        <FontAwesomeIcon icon={faEdit} />
                      </button>
                      {"   "}
                      <button disabled className="btn btn-danger">
                        <FontAwesomeIcon icon={faTrashAlt} />
                      </button>
                    </td>
                  )}
                </tr>
              );
            })}
          </tbody>
        </table>

        <Modal isOpen={this.state.updateModal}>
          <ModalHeader style={{ display: "block" }}>
            <span style={{ float: "left" }}>Update User</span>
            <span
              style={{ float: "right", cursor: "pointer" }}
              onClick={() => this.updateModal()}
            >
              x
            </span>
          </ModalHeader>
          <ModalBody>
            <div className="form-group">
              <form onSubmit={this.patchReq}>
                <label htmlFor="id">ID</label>
                <input
                  className="form-control"
                  type="number"
                  name="id"
                  id="id"
                  disabled
                  value={form ? form.id : ""}
                />
                <br />
                <label htmlFor="email">Email</label>
                <input
                  className="form-control"
                  type="email"
                  name="email"
                  id="email"
                  required
                  onChange={this.handleChange}
                  value={form ? form.email : ""}
                />
                <br />
                <label htmlFor="password">Password</label>
                <input
                  className="form-control"
                  type="password"
                  name="password"
                  id="password"
                  required
                  onChange={this.handleChange}
                  value={form ? form.password : ""}
                  minLength={4}
                />
                <br />
                <label htmlFor="name">Name</label>
                <input
                  className="form-control"
                  type="text"
                  name="name"
                  id="name"
                  required
                  onChange={this.handleChange}
                  value={form ? form.name : ""}
                  minLength={1}
                />
                <br />
                <label htmlFor="age">Age</label>
                <input
                  className="form-control"
                  type="number"
                  name="age"
                  id="age"
                  min="1"
                  max="99"
                  required
                  onChange={this.handleChange}
                  value={form ? form.age : ""}
                />
                <br />
                <label htmlFor="address">Address</label>
                <input
                  className="form-control"
                  type="text"
                  name="address"
                  id="address"
                  minLength={4}
                  onChange={this.handleChange}
                  value={form ? form.address : ""}
                />
                <br />

                <ModalFooter>
                  <button type="submit" className="btn btn-primary">
                    Update
                  </button>

                  <button
                    type="reset"
                    className="btn btn-danger"
                    onClick={() => this.updateModal()}
                  >
                    Cancel
                  </button>
                </ModalFooter>
              </form>
            </div>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.deleteModal}>
          <ModalBody>
            Are you sure to delete the user {form && form.name} ?
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-danger" onClick={() => this.deleteReq()}>
              Yes
            </button>
            <button
              className="btn btn-secundary"
              onClick={() => this.setState({ deleteModal: false })}
            >
              No
            </button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Users;
