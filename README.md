
# Fathom3 Mariano Developer Task

**Content**

- [Introduction](#introduction)
- [Docker-compose](#docker-compose(faster-installation))
- [Manual Installation](#manual-installation)
- [Usage](#usage)

## **Introduction**

In this developer task, the following specifications given in the document have been developed:

A NodeJs API + Fastify with typescript, prisma (ORM), Postgresql (DB):\
The API contains a CRUD of users + Login, all accessible from swagger.

A ReactJs Web with typescript:\
The Web contains user registration, login to enter the web, a control panel with a table to see all the users, and edit / delete the current user logged.

.env files will be uploaded for faster deployment

#### To deploy the following applications can be done in the following two ways:

## **Docker-compose (Faster Installation)**

The following containers will be created:

- db_task_fathom3_mariano (**postgresql**) *(Default PORT **49152**)*
- api_task_fathom3_mariano (**api**) *(Default PORT **49153**)*
- web_task_fathom3_mariano (**web**) *(Default PORT **49154**)*

In the parent folder path is the docker-compose.yml file.

You won't need to change anything in the docker-compose.yml unless you have the ports **49152**, **49153** and **49154** in use.

Run one of these commands depending on the docker compose you have installed on the system:

```
docker-compose up -d 
```
```
docker compose up -d 
```

To stop the container service, run one of the followings commands in same path:

```
docker-compose down
```
```
docker compose down
```

### Docker Prisma migrate db

After docker compose up:

It is important to migrate db with prisma in the api container, since when building with docker compose it could not be done from the api Dockerfile because postgresql is not started at that moment.
```
docker exec -it api_task_fathom3_mariano npx prisma migrate dev
docker restart api_task_fathom3_mariano
```

It only has to be done once, if reboots are needed the changes will be saved in postgres_task_mariano_db volume.

## **Manual Installation**

**API:**

It is important to change the environment variables for the connection with postgresql.\
In the api folder you have to run the following commands for the installation:
```
npm install
npx prisma migrate dev
```

*Run Api*
```
npm run tsc
npm start
```

**WEB:**

It is important to change the environment variables for the API connection\
In the web folder you have to run the following commands for the installation:
```
npm install
```

*Run Web*
```
npm start
```

## **Usage**
Depending on which installation has been made, the port of each one must be keep in mind.

**SWAGGER API:**
- Host: http://HOST:PORT/docs

**WEBSITE:**
- Host: http://HOST:PORT
